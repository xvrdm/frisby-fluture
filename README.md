# frisby-fluture

(Tentative) Rewrite of "Classroom Coding with Prof. Frisby" using React Hooks and Fluture.

Original videos from Brian Lonsdorf can be found [here](https://www.youtube.com/watch?v=h_tkIpwbsxY).
