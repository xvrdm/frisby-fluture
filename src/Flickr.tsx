import React, { useState } from "react";
import { flickrGetPhotos } from "./frontend/model";
import { fork } from "fluture";
import { map } from "ramda";
import { Nothing } from "./frontend/maybe";

export const Flickr = (props) => {
  const { showError } = props;
  const [term, setTerm] = useState("");
  const [results, setResults] = useState([]);

  // handleTermChange :: Event -> State term
  const handleTermChange = ({ currentTarget: t }) => {
    setTerm(t.value);
  };

  const updateResults = (xs) => {
    setResults(xs);
  };

  // searchClicked :: Event -> Futute Error JSON
  const searchClicked = (_) =>
    flickrGetPhotos(term).pipe(fork(showError)(updateResults));

  const onDragStart = ({
    dataTransfer: dt,
    currentTarget: t,
    clientX: x,
    clientY: y,
  }) => {
    // console.log(t);
    // console.log(t.getBoundingClientRect());
    // console.log(x, y);
    dt.setData("text", t.src);
    dt.setData("shiftX", x - t.getBoundingClientRect().left);
    dt.setData("shiftY", y - t.getBoundingClientRect().top);
  };

  const getImgTag = (p) => (
    <img src={p.src} key={p.src} draggable={true} onDragStart={onDragStart} />
  );
  const imgs = map(getImgTag)(results);

  return (
    <div id="flickr">
      <input value={term} onChange={handleTermChange} />
      <button onClick={searchClicked}>Search</button>
      <div id="results">{imgs}</div>
    </div>
  );
};
