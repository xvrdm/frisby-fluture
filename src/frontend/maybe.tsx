const VALUE = Symbol("Value");

class Container {
  constructor(x) {
    this[VALUE] = x;
  }

  map(f) {
    return f(this[VALUE]);
  }

  fold(f, g) {
    return g(this[VALUE]);
  }
}

class Functor extends Container {
  static of(x) {
    return new Functor(x);
  }

  map(f) {
    return Functor.of(f(this[VALUE]));
  }
}

export class Nothing extends Functor {
  static of(x) {
    return new Nothing(x);
  }

  isNothing() {
    return true;
  }

  map(f) {
    return Nothing.of(this[VALUE]);
  }

  fold(f, g) {
    return f();
  }
}

export class Just extends Functor {
  static of(x) {
    return new Just(x);
  }

  isNothing() {
    return false;
  }

  map(f) {
    return Maybe.of(f(this[VALUE]));
  }

  fold(f, g) {
    return g(this[VALUE]);
  }
}

export class Maybe extends Functor {
  constructor(x) {
    return x === undefined || x === null ? new Nothing() : new Just(x);
  }

  static of(x) {
    return new Maybe(x);
  }
}
