import * as F from "fluture";
import { getJSON } from "jquery";
import { curry } from "ramda";
import { Just, Nothing } from "./maybe";
import { Url } from "./model";

export const Http = {
  // get :: Url -> Future Error JSON
  get: (url: Url) =>
    F.Future((rej, res) => {
      getJSON(url).fail(rej).done(res);
      return () => {};
    }),
};

// preventDefault :: Event -> State Event
export const preventDefault = (e) => e.preventDefault();

// indexOf :: a -> [a] -> Maybe Number
export const indexOf = curry((x, xs) => {
  const idx = xs.indexOf(x);
  return idx < 0 ? Nothing.of(null) : Just.of(idx);
});
