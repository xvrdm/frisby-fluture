import { compose, prop, map, curry, remove, append } from "ramda";
import * as F from "fluture";
import daggy from "daggy";
import { Http, indexOf } from "./utils";

export type Url = string;
export type Point = number;

export const Photo = daggy.tagged("Point", ["src", "x", "y"]);
const newPhoto = (url) => Photo(url, 0, 0);

const HOST = "api.flickr.com";
const PATH = "/services/feeds/photos_public.gne";

// makeQuery :: String -> String
const makeQuery = (t: string): string =>
  `?tags=${t}&format=json&jsoncallback=?`;
// makeUrl :: String -> Url
const makeUrl = (t: string): Url => `https://${HOST}${PATH}${makeQuery(t)}`;

// extractUrls :: JSON -> [Url]
const extractUrls = compose(
  map(compose(prop("m"), prop("media"))),
  prop("items")
);

// flickrSearch :: String -> Future Error JSON
export const flickrSearch = compose(Http.get, makeUrl);
// flickrGetUrls :: String -> Future Error [Photo]
export const flickrGetPhotos = (t) =>
  flickrSearch(t)
    .pipe(F.map(extractUrls))
    .pipe(F.map(map(newPhoto)));

// indexOfPhoto :: Photo -> [Photo] -> Maybe Number
const indexOfPhoto = curry((p, ps) => indexOf(p.src, ps.map(prop("src"))));

// replacePhoto :: Photo -> [Photo] -> [Photo]
export const replacePhoto = curry((p, ps) =>
  indexOfPhoto(p)(ps)
    .map((i) => remove(i, 1, ps))
    .fold(() => append(p, ps), append(p))
);
