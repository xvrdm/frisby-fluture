import React, { useState } from "react";
import "./App.css";
import { Collage } from "./Collage";
import { Flickr } from "./Flickr";

const App = () => {
  const [error, setError] = useState("");

  return (
    <div id="app">
      {error && <p>{error}</p>}
      <Flickr showError={setError} />
      <Collage showError={setError} />
    </div>
  );
};

export default App;
