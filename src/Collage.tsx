import React, { useState } from "react";
import { append, map } from "ramda";
import { Photo, replacePhoto } from "./frontend/model";
import { preventDefault } from "./frontend/utils";

export const Collage = (props) => {
  const { showError } = props;
  const [photos, setPhotos] = useState([]);

  // onDrop :: Event -> State Photos
  const onDrop = ({
    dataTransfer: dt,
    clientX: x,
    clientY: y,
    currentTarget: t,
  }) => {
    const offsetY = t.getBoundingClientRect().top;
    const offsetX = t.getBoundingClientRect().left;
    const src = dt.getData("text");
    const shiftX = dt.getData("shiftX");
    const shiftY = dt.getData("shiftY");
    console.log(shiftX);
    console.log(shiftY);
    const photo = Photo(src, x - offsetX - shiftX, y - offsetY - shiftY);
    setPhotos((photos) => replacePhoto(photo, photos));
  };
  const getImgTag = (p) => (
    <img src={p.src} key={p.src} style={{ top: p.y, left: p.x }} />
  );
  const imgs = map(getImgTag)(photos);

  console.log(photos);
  return (
    <div id="collage" onDrop={onDrop} onDragOver={preventDefault}>
      <div id="photos">{imgs}</div>
    </div>
  );
};
